#!/usr/bin/python

import os
import hashlib
import paramiko
import re
import stat
import time
import mysql.connector
import multiprocessing
from datetime import datetime, timedelta
import calendar
from mysql.connector import errorcode
import signal
import random
import logging
import shlex
import subprocess
import smtplib
from email.mime.text import MIMEText
import socket

maxThreads = 5 # Maximum number of simultaneous threads
maxThreadExecTime = 1200 # Maximum number of minutes a thread can run
maxExecs = 1 # Maximum number of simultaneous exec processes
sleepTime = 60 # Interval in seconds for wait between threads execution
downloadTime = 300 # Interval in seconds to wait for files downloading

# Set to 0 if you don't want to receive emails
# Set to 1 if you want to receive all emails
# Set to 2 if you want to receive error emails
sendEmail = 0 # Set this to 1 if you want to receive emails with actions status

jobs = {}
execs = {}
svcsPool = {}

INPUTFILE = "/var/log/jumpstart/server.log"

# Get the Logs location where to save log files
LogDefault = "/var/log/jumpstart/"
LogDir = os.getenv('LOGS', LogDefault)

sqlconfig = {
	'user': 'username',
	'password': 'password',
	'host': '127.0.0.1',
	'database': 'makofeeds',
	'raise_on_warnings': True,
}

# Do database connection
try:
	cnx = mysql.connector.connect(**sqlconfig)

except mysql.connector.Error as err:
	if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
		errorMsg = 'Something is wrong with your DB user name or password'
		makoLogger.critical(errorMsg)
		doEmail('DB Error', errorMsg)
		# 21
		raise SystemExit(err.errno)
	elif err.errno == errorcode.ER_BAD_DB_ERROR:
		errorMsg = 'Database does not exist'
		makoLogger.critical(errorMsg)
		doEmail('DB Error', errorMsg)
		# 25
		raise SystemExit(err.errno)
	else:
		makoLogger.critical(err)
		# 211 unable to connect
		# 20 Access denied
		doEmail('DB Error', err)
		raise SystemExit(err.errno)

cursor = cnx.cursor()
# Create a logerrors array to use in SVC2LOGS entries
query = "SELECT id, name FROM logerrortypes"
cursor.execute(query)
allRows = cursor.fetchall()
logerrors = {}
for errNo, name in allRows:
	logerrors[name] = errNo

cnx.close()
cursor.close()

def doEmail(emlSubject, emlBody, emailType):
	if sendEmail == 1 or emailType == sendEmail or emailType == 0:
		vHostname = socket.gethostname()
		msg = MIMEText(emlBody)
		msg['From'] = '{} MakoFeedsProcessor <no-reply@{}.jumpstartservice.com>'.format(vHostname, vHostname)
		msg['To'] = '<support@jumpstartwireless.com>'
		msg['Subject'] = emlSubject

		try:
			smtp = smtplib.SMTP('mail.jumpstartdemo.com')
			smtp.sendmail(msg['From'], msg['To'], msg.as_string())
			smtp.quit()
			errorMsg = 'Email with subject: {} Sent'.format(emlSubject)
			makoLogger.info(errorMsg)
		except:
			errorMsg = 'Error, the message could not be sent'
			makoLogger.error(errorMsg)
	else:
		makoLogger.debug('Not going to send email because variable is not configured to do so.')

def doUpdate(updTable, updSentence, updWhere):
	try:
		cnx = mysql.connector.connect(**sqlconfig)

	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			errorMsg = 'Something is wrong with your DB user name or password'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 21
			raise SystemExit(err.errno)
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			errorMsg = 'Database does not exist'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 25
			raise SystemExit(err.errno)
		else:
			makoLogger.critical(err)
			# 211 unable to connect
			# 20 Access denied
			doEmail('DB Error', errorMsg, 2)
			raise SystemExit(err.errno)

	doAgain = 1
	cursor = cnx.cursor()
	query = "UPDATE {} SET {}, last_execution = NOW()".format(updTable, updSentence)
	if updWhere:
		query += " WHERE {}".format(updWhere)

	while doAgain == 1:
		doAgain = 0
		try:
			cursor.execute(query)
			affected_count = cursor.rowcount
			cnx.commit()
			errorMsg = '{} returned {} affected rows'.format(query, affected_count)
			makoLogger.debug(errorMsg)
		except Exception, e:
			errorMsg = 'There was an error when executing query {}. ({})'.format(query, e)
			makoLogger.error(errorMsg)
			doAgain = 1
	cursor.close()
	cnx.close()

def doLog(logErrorType, logExternalIDs, logResultCode, logResultString):
	# logErrorType:
	# 0 => OK
	# 1 => Ready To Run
	# 2 => Running
	# 3 => Error
	try:
		cnx = mysql.connector.connect(**sqlconfig)

	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			errorMsg = 'Something is wrong with your DB user name or password'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 21
			raise SystemExit(err.errno)
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			errorMsg = 'Database does not exist'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 25
			raise SystemExit(err.errno)
		else:
			makoLogger.critical(err)
			# 211 unable to connect
			# 20 Access denied
			doEmail('DB Error', err, 2)
			raise SystemExit(err.errno)
	cursor = cnx.cursor()

	query = 'INSERT INTO svc2logs(logerrortype_id, result_code, result_string'
	for id in logExternalIDs:
		query += ', {}'.format(id)

	query += ') VALUES({}, {}, \'{}\''.format(logErrorType, logResultCode, logResultString)
	for id in logExternalIDs:
		query += ', {}'.format(logExternalIDs[id])

	query += ')'
	try:
		logResultString = logResultString.replace("'", "")
	except Exception, e:
		errorMsg = 'There was an error replacing \' in the Result String: {}'.format(e)
		makoLogger.error(errorMsg)

	try:
		cursor.execute(query)
		cnx.commit()
	except Exception, e:
		errorMsg = 'There was an error executing query: {}'.format(query)
		makoLogger.error(errorMsg)

	cursor.close()
	cnx.close()
				
def doDownload(arrSvcs):
	errorMsg = 'Starting Download Process for Service: {}'.format(arrSvcs['service_id'])
	makoLogger.info(errorMsg)
	# Variables used to filename replacement
	vDateTime = datetime.now()
	longYear = vDateTime.strftime('%Y')
	shortYear = vDateTime.strftime('%y')
	vMonth = vDateTime.strftime('%m')
	vDay = vDateTime.strftime('%d')
	# Calculate the time to download the file
	vNow = datetime.now()
	vDays = vNow - timedelta(seconds=(60 * 60 * 24 * 2))

	# Do database connection
	try:
		cnx = mysql.connector.connect(**sqlconfig)

	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			errorMsg = 'Something is wrong with your DB user name or password'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 21
			raise SystemExit(err.errno)
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			errorMsg = 'Database does not exist'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 25
			raise SystemExit(err.errno)
		else:
			makoLogger.critical(err)
			# 211 unable to connect
			# 20 Access denied
			doEmail('DB Error', err, 2)
			raise SystemExit(err.errno)

	cursor = cnx.cursor()
	# service_id is going to be used multiple times in this function
	service_id = '{}'.format(arrSvcs['service_id'])
	errorMsg = 'Setting service_status to 2 (Running) for Service: {}'.format(service_id)
	makoLogger.info(errorMsg)
	# Update SERVICES SET service_status to RUNNING
	doUpdate('services', 'status = 2', 'id = ' + service_id)

	errorMsg = "Started downloading process"
	arrIDs = {}
	arrIDs['service_id'] = service_id
	doLog(logerrors['General'], arrIDs, 2, errorMsg)

	# vSvcError indicates if there have been errors downloading all files for this service
	vSvcError = 0
	for fileID in arrSvcs['files']:
		errno = 1
		myFileID = '{}'.format(fileID)
		arrIDs['svc2file_id'] = myFileID
		# errno = 0 (Everything completed successfully)
		# errno = 1 (File was not found)
		# errno = 3 (Some error raised)
		# Verify file was not picked up for another process omit if file is on hold since more than 15 minutes
		query = "SELECT COUNT(*) AS qty, IF(DATE_ADD(last_execution, INTERVAL 15 MINUTE) > NOW(),0,1) AS future_last_execution, last_execution FROM svc2files WHERE id = {} AND status = 2".format(fileID)
		cursor.execute(query)
		allRows = cursor.fetchall()
		vContinue = 1
		for vRow in allRows:
			if vRow[0] == 1 and vRow[1] == 0:
				vContinue = 0
				errorMsg = 'The file {} has been picked by another process less than 15 minutes ago ({}). Will skip processing'.format(fileID, vRow[2])
				makoLogger.debug(errorMsg)
			elif vRow[0] == 1 and vRow[1] == 1:
				errorMsg = 'The file {} has already been picked by another process over 15 minutes ago ({}) and left orphan. Resetting last_execution time and continue processing'.format(fileID, vRow[2])
				makoLogger.debug(errorMsg)
			else:
				errorMsg = 'The file {} is ready to be picked by this process'.format(fileID)
				makoLogger.info(errorMsg)

		# If no records were found from previous query means that the file is OK to be downloaded
		# Replace date regexp in file pattern with today's date values
		tmpFileName = arrSvcs['files'][fileID]['svc2file_filename']
		#tmpFileName = tmpFileName.replace('%{YYYY}', longYear)
		#tmpFileName = tmpFileName.replace('%{YY}', shortYear)
		#tmpFileName = tmpFileName.replace('%{MM}', vMonth)
		#tmpFileName = tmpFileName.replace('%{DD}', vDay)
		vPrevPeriod = re.sub(r'}.*$', '', re.sub(r'^.*%{DD', '', tmpFileName))
		vPrevPeriod = vPrevPeriod.replace('-', '')
		if vPrevPeriod:
			vPrevDateTime = vDateTime - timedelta(int(vPrevPeriod))
			vPrevlongYear = vPrevDateTime.strftime('%Y')
			vPrevshortYear = vPrevDateTime.strftime('%y')
			vPrevMonth = vPrevDateTime.strftime('%m')
			vPrevDay = vPrevDateTime.strftime('%d')
			tmpFileName = tmpFileName.replace('%{YYYY}', vPrevlongYear)
			tmpFileName = tmpFileName.replace('%{YY}', vPrevshortYear)
			tmpFileName = tmpFileName.replace('%{MM}', vPrevMonth)
			vReplaceString = "%{DD-"
			vReplaceString += vPrevPeriod
			vReplaceString += "}"
			tmpFileName = tmpFileName.replace(vReplaceString, vPrevDay)
		else:
			tmpFileName = tmpFileName.replace('%{YYYY}', longYear)
			tmpFileName = tmpFileName.replace('%{YY}', shortYear)
			tmpFileName = tmpFileName.replace('%{MM}', vMonth)
			tmpFileName = tmpFileName.replace('%{DD}', vDay)

		# downloadFiles is going to be used later to be compared with server files and be downloaded
		downloadFiles = tmpFileName.lower()

		if vContinue == 1:
			if arrSvcs['files'][fileID]['svc2file_enabled'] == 0:
				# File is disabled. No need to download but we should set errno to 0 to continue with execs process.
				errno = 0
				errorMsg = "Omitting download of {} as is disabled".format(arrSvcs['files'][fileID]['svc2file_filename'])
				makoLogger.warn(errorMsg)
				doLog(logerrors['File'], arrIDs, 2, errorMsg)
				doUpdate('svc2files', 'force_run = 0, status = 0', 'id = ' + myFileID)
			else:
				# File is enabled. Continue with download process.
				errorMsg = "Started file downloading"
				makoLogger.info('Setting svc2files 2 for id: {}'.format(myFileID))
				doLog(logerrors['File'], arrIDs, 2, errorMsg)
				# Update SVC2FILES to indicate files in queue are being downloaded
				doUpdate('svc2files', 'status = 2', 'id = ' + myFileID)

				# Save extraExtension variable. It is going to be used later in the function
				extraExtension = arrSvcs['files'][fileID]['svc2file_add_extension']

				# Format vNow to be used in log files
				vNow = datetime.now().strftime('%Y%m%d-%H%M%S')

				# Save paramiko log to file
				strLogFile = '{}/sftpifp.{}.{}.log'.format(LogDir, arrSvcs['service_name'], vNow)
				paramiko.util.log_to_file(strLogFile)
				logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S', filename=LogDir + '/' + arrSvcs['service_logfile'],level=logging.WARNING)

				# Local files directory are stored in myFilesDir
				myFilesDir = arrSvcs['files'][fileID]['svc2file_localdir']
				# Hostname of the remote machine to connect
				hostname = arrSvcs['service_hostname']
				# Port number to use to connect
				port = arrSvcs['service_port']
				# Username of the remote machine
				username = arrSvcs['service_username']
				# Password of the remote machine
				password = arrSvcs['service_password']
				# Remote dir where to get the files from
				remoteDir = arrSvcs['files'][fileID]['svc2file_remotedir']
				# List local directory
				myLocalFile = os.listdir(myFilesDir)
				# Array of md5
				myLocalMd5 = []

				# Fill md5 array with the md5sum values of local files
				for localfile in myLocalFile:
					infile = open('{}/{}'.format(myFilesDir, localfile), 'rb')
					content = infile.read()
					infile.close()
					m = hashlib.md5()
					m.update(content)
					md5 = m.hexdigest()
					myLocalMd5.append(md5)

				hostkeytype = None
				hostkey = None
				# Read host keys from known_hosts file
				host_keys = paramiko.util.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))

				if hostname in host_keys:
					hostkeytype = host_keys[hostname].keys()[0]
					hostkey = host_keys[hostname][hostkeytype]
#					print 'Using host key of type %s' % hostkeytype

				# Create the transport to connect to the remote server
				try:
					t = paramiko.Transport(hostname, port)
				except Exception, e:
					errorMsg = '*** Caught exception: {}: {}'.format(e.__class__, e)
					makoLogger.error(errorMsg)
					doLog(logerrors['Connect'], arrIDs, 3, e)
					doUpdate('svc2files', 'status = 3', 'id = ' + myFileID)
					errno = 3
					vSvcError = 3
					doEmail('Transport Error', errorMsg, 2)

				# If there was no error creating the transport
				if errno == 1:
					# Connect to the remote server
					try:
						t.connect(username=username, password=password, hostkey=hostkey)
					except Exception, e:
						errorMsg = '*** Caught exception: {}: {}'.format(e.__class__, e)
						makoLogger.error(errorMsg)
						doLog(logerrors['Connect'], arrIDs, 3, e)
						doUpdate('svc2files', 'status = 3', 'id = ' + myFileID)
						errno = 3
						vSvcError = 3
						doEmail('Connection Error', errorMsg, 2)

					# If the connection was successful
					if errno == 1:
						# Create paramiko SFTP transport
						sftp = paramiko.SFTPClient.from_transport(t)
						# Check if remote directory can be listed
						try:
							dirContents = sftp.listdir_attr(remoteDir)
						except Exception, e:
							errorMsg = '*** Caught exception: {}: {}'.format(e.__class__, e)
							makoLogger.error(errorMsg)
							doLog(logerrors['Transfer'], arrIDs, 3, e)
							doUpdate('svc2files', 'status = 3', 'id = ' + myFileID)
							errno = 3
							vSvcError = 3
							doEmail('Transfer Error', errorMsg, 2)

						# If directory was listed properly and contents were saved to dirContents
						if errno == 1:
							# vFound indicates if the file was found
							vFound = 0
							# Walk through dirContents to verify if there's any file to download
							for item in dirContents:
								# If file is newer than vDays
								if datetime.fromtimestamp(item.st_mtime) > vDays:
									# Get file type (file or directory)
									kind = stat.S_IFMT(item.st_mode)
									# If is file and match file name
									if kind == stat.S_IFREG and re.match(downloadFiles, item.filename.lower()):
										# The file was found
										vFound = 1
										# If file already exists in the directory
										if os.path.exists(myFilesDir + "/" + item.filename + extraExtension):
											# Get file attributes
											myFile = '{}/{}{}'.format(myFilesDir, item.filename, extraExtension)
											lf_mode, lf_ino, lf_dev, lf_nlink, lf_uid, lf_gid, lf_size, lf_atime, lf_mtime, lf_ctime = os.stat(myFile)
											# IF modification time from existing file differs to file in server
											if item.st_mtime > lf_mtime or item.st_size != lf_size:
												errorMsg = "File {} already exists but modification time differs: {} - {}\n".format(item.filename, item.st_mtime, lf_mtime)
												makoLogger.warn(errorMsg)
												# Insert into SVC2LOGS informing the error
												errorMsg = "Local File: {}/{}{} (mt: {} - s: {}) and Remote File: {} (mt: {} - s: ) modification time (mt) or size (s) differs".format(myFilesDir, item.filename, extraExtension, lf_mtime, lf_size, item.filename, item.st_mtime, item.st_size)
												doLog(logerrors['Transfer'], arrIDs, 3, errorMsg)
												makoLogger.debug('Going to get {}/{} into {}/{}{}'.format(remoteDir, item.filename, myFilesDir, item.filename, extraExtension))
												# Download the file
												vRemoteFile = '{}/{}'.format(remoteDir, item.filename)
												vLocalFile = '{}/{}{}'.format(myFilesDir, item.filename, extraExtension)
												try:
													sftp.get(vRemoteFile, vLocalFile)
												except Exception, e:
													errorMsg = '*** Caught exception: {}: {}'.format(e.__class__, e)
													makoLogger.error(errorMsg)
													# insert into SVC2LOGS informing the resulting status
													doLog(logerrors['Transfer'], arrIDs, 3, e)
													errno = 3
													vSvcError = 3
													doEmail('Transfer Error', errorMsg, 2)
												else:
													errorMsg = "The file {} was downloaded and saved as {}/{}{}".format(item.filename, myFilesDir, item.filename, extraExtension)
													doLog(logerrors['Transfer'], arrIDs, 0, errorMsg)
													errno = 0
													makoLogger.info(errorMsg)
											else:
												errorMsg = "File {} is the same as {}/{}{}".format(item.filename, myFilesDir, item.filename, extraExtension)
												makoLogger.warn(errorMsg)
												errno = 0
										else:
											# File doesn't exist in the local directory
											errorMsg = "Going to get {}/{} into {}/{}{}".format(remoteDir, item.filename, myFilesDir, item.filename, extraExtension)
											makoLogger.debug(errorMsg)
											# Download the file
											try:
												sftp.get(remoteDir + "/" + item.filename, myFilesDir + "/" + item.filename + extraExtension)
											except Exception, e:
												errorMsg = '*** Caught exception: {}: {}'.format(e.__class__, e)
												makoLogger.error(errorMsg)
												doLog(logerrors['Transfer'], arrIDs, 3, e)
												errno = 3
												vSvcError = 3
												doEmail('Transfer Error', errorMsg, 2)
											else:
												errorMsg = "The file {} was downloaded and saved as {}/{}{}".format(item.filename, myFilesDir, item.filename, extraExtension)
												doLog(logerrors['Transfer'], arrIDs, 0, errorMsg)
												errno = 0
												makoLogger.info(errorMsg)
							# If file was not found
							if vFound == 0:
								# errno is still 1, so file status will be set to 1 (Ready to Run)
								# Next time service is checked file will be considered to be downloaded
								errorMsg = 'File {} was not found'.format(downloadFiles)
								doLog(logerrors['File'], arrIDs, 3, errorMsg)
								makoLogger.error(errorMsg)
							strErrno = '{}'.format(errno)
							# SVC2FILES is going to change its status from RUNNING to:
							# Update SVC2FILES and SET the resulting value (0 if OK, 1 if READY TO RUN and 3 if ERROR)
							if errno == 0:
								doUpdate('svc2files', 'force_run = 0, status = ' + strErrno, 'id = ' + myFileID)
							else:
								doUpdate('svc2files', 'status = ' + strErrno, 'id = ' + myFileID)

					# Close transport
					t.close
			# If There were no errors
			if errno == 0:
				# As there were no errors we can move ahead to exec process.
				#query = "SELECT svc2exec_id FROM execs2files WHERE svc2file_id = {} AND enabled = 1".format(myFileID)
				#cursor.execute(query)
				#allExecs = cursor.fetchall()
				#for execid in allExecs:
				#	svc2exec_id = '{}'.format(execid[0])
				#	doUpdate('svc2execs', 'status = 1', 'id = ' + svc2exec_id)
				errorMsg = 'Finished downloading file {} with no errors.'.format(downloadFiles)
				doLog(logerrors['File'], arrIDs, 0, errorMsg)
				makoLogger.info(errorMsg)
			else:
				# Maybe errno is 1 (Ready to Run). That means an error because no action was recquired
				# There were errors downloading files
				strErrno = '{}'.format(errno)
				# Update SERVICES and SET service_status to 3
				#doUpdate('services', 'service_status = ' + strErrno, 'service_id = ' + service_id)
				errorMsg = 'Finished downloading file {} with errors.'.format(downloadFiles)
				doLog(logerrors['File'], arrIDs, 3, errorMsg)
				makoLogger.error(errorMsg)
				vSvcError = 3
				doEmail('Transfer Error', errorMsg, 2)
		else:
			errorMsg = '{} file is already taken by another process. Will not download'.format(downloadFiles)
			makoLogger.warn(errorMsg)

	# If all files downloaded properly
	if vSvcError == 0:
		# SET everything for execs
		# service_file_status = 0 (OK), service_exec_status = 1 (Ready to Run)
		# svc2exec_status = 1 (Ready to Run)
		errorMsg = 'Download process finished successfully for service: {}'.format(service_id)
		doLog(logerrors['General'], arrIDs, 0, errorMsg)
		doUpdate('services', 'status = 0, file_status = 0, exec_status = 1', 'id = ' + service_id)
		#doUpdate('svc2execs', 'status = 1', 'service_id = ' + service_id)
		makoLogger.info('Finished downloading files successfully. Will update the file status to 0 to indicate success')
		doEmail('Transfers Success', errorMsg, 1)
	else:
		# Not all files were downloaded properly.
		errorMsg = 'Download process finished unsuccessfully for service: {}'.format(service_id)
		doLog(logerrors['General'], arrIDs, 3, errorMsg)
		# SET service_file_status = 3 (ERROR), service_exec_status = 0
		doUpdate('services', 'status = 3, file_status = 3, exec_status = 0', 'id = ' + service_id)
		makoLogger.error('Finished downloading files with errors. Will update the file status to 3 to indicate an error')
		doEmail('Transfers Error', errorMsg, 2)

def findExecs(vService):
	makoLogger.debug('Finding Execs routine for Service: {}'.format(vService))
	try:
		cnx = mysql.connector.connect(**sqlconfig)

	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			errorMsg = 'Something is wrong with your DB user name or password'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 21
			raise SystemExit(err.errno)
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			errorMsg = 'Database does not exist'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 25
			raise SystemExit(err.errno)
		else:
			makoLogger.critical(err)
			# 211 unable to connect
			# 20 Access denied
			doEmail('DB Error', err, 2)
			raise SystemExit(err.errno)

	cursor = cnx.cursor()

	# Query to check FILES that matches SERVICE
	# arrFiles is going to hold:
	# arrFiles[vFileId] = vFileStatus
	arrFiles = {}

	query = "SELECT s2f.id, s2f.status FROM svc2files AS s2f, services AS svc WHERE s2f.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND s2f.last_execution <= CONCAT(DATE(NOW()), ' ', svc.ending_time) AND s2f.service_id = {} AND svc.id = s2f.service_id".format(vService)
	makoLogger.debug("Query for fetching file status for svc {}: {}".format(vService, query))
	cursor.execute(query)
	allRows = cursor.fetchall()

	# If previous query returns any records
	if len(allRows) > 0:
		makoLogger.debug('Fetching files info for svc: {}'.format(vService))
		for row in allRows:
			#makoLogger.debug('Return is: {}'.format(row))
			vFileId = row[0]
			vFileStatus = row[1]
			arrFiles[vFileId] = vFileStatus
			makoLogger.debug('Setting arrFiles[{}] = {} for svc: {}'.format(vFileId, vFileStatus, vService))

	# Query to check EXECS that matches SERVICE
	# arrExecs is going to hold:
	# arrExecs[vExecId]['status']
	# arrExecs[vExecId]['parent_id']

	query = "SELECT s2e.id, s2e.status, s2e.parent_id FROM svc2execs AS s2e LEFT JOIN services AS svc ON(svc.id = s2e.service_id) WHERE ((s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND s2e.status IN(1, 3) AND s2e.last_execution <= CONCAT(DATE(NOW()), ' ', svc.ending_time)) OR (s2e.last_execution <= CONCAT(DATE(NOW()), ' ', svc.starting_time) OR s2e.force_run = 1)) AND s2e.service_id = {} AND s2e.enabled = 1 ORDER BY s2e.last_execution, s2e.parent_id, s2e.priority ASC".format(vService)

	makoLogger.debug("Query for fetching execs status for svc {}: {}".format(vService, query))
	cursor.execute(query)
	allRows = cursor.fetchall()

	# If previous query returns any records
	if len(allRows) > 0:
		makoLogger.debug('Fetching execs info for svc {}'.format(vService))
		arrExecs = {}
		separator = ""
		vExecConditional = ""
		for row in allRows:
			vExecId = row[0]
			vExecStatus = row[1]
			vExecParentId = row[2]
			arrExecs[vExecId] = {'status': vExecStatus, 'parent_id': vExecParentId}
			makoLogger.debug('Setting arrExecs[{}] = status: {}, parent_id: {} for svc: {}'.format(vExecId, vExecStatus, vExecParentId, vService))
			vExecConditional += "{}{}".format(separator, vExecId)
			separator = ", "

		# Query to check EXECS2FILES that matches svc2exec_id
		# vExecs2Files is going to hold:
		# vExecs2Files[vExecId] = fileStatus

		query = "SELECT e2f.svc2exec_id AS s2eid, e2f.svc2file_id AS s2fid, e2f.active AS e2factive FROM execs2files AS e2f LEFT JOIN svc2execs AS s2e ON(s2e.id = e2f.svc2exec_id) WHERE e2f.svc2exec_id IN({}) AND e2f.enabled = 1 ORDER BY s2e.parent_id ASC".format(vExecConditional)

		makoLogger.debug("Query for execs2files status for svc {}: {}".format(vService, query))

		cursor.execute(query)
		allRows = cursor.fetchall()

		# If previous query returns any records
		vExecs2Files = {}
		if len(allRows) > 0:
			makoLogger.debug('Analyzing results for svc: {}'.format(vService))
			for row in allRows:
				vExecId = row[0]
				vFileId = row[1]
				vActive = row[2]
				makoLogger.debug('Looking for exec: {} and file: {} for svc: {}'.format(vExecId, vFileId, vService))

				if vExecId in arrExecs:
					makoLogger.debug('vExecId {} Found in arrExecs for svc: {}'.format(vExecId, vService))
				else:
					makoLogger.debug('Not found vExecId: {} for svc: {}'.format(vExecId, vService))

				try:
					if vFileId in arrFiles:
						makoLogger.debug('vFileId {} Found in arrFiles for svc: {}'.format(vFileId, vService))
					else:
						makoLogger.debug('Not found vFileId: {} for svc: {}'.format(vFileId, vService))
				except:
					makoLogger.error('File Exception for vFileId: {}, svc: {}, exec: {}'.format(vFileId, vService, vExecId))
					pass

				if vActive == 1:
					if vExecId in arrExecs and vFileId in arrFiles:
						makoLogger.debug('vExecId: {} and vFileId: {} found in arrExecs and arrFiles for svc: {}'.format(vExecId, vFileId, vService))
						parent_id = arrExecs[vExecId]['parent_id']

						vcontinue = 0
						if parent_id != 0:
							query = "SELECT s2e.status FROM svc2execs AS s2e LEFT JOIN services AS svc ON(svc.id = s2e.service_id) WHERE s2e.id = {} AND s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND s2e.last_execution <= CONCAT(DATE(NOW()), ' ', svc.ending_time) AND s2e.enabled = 1 AND s2e.status = 0".format(parent_id)

							makoLogger.debug("Query for svc {}: {}".format(vService, query))
							cursor.execute(query)
							parentRows = cursor.fetchall()

							# If previous query returns any records
							if len(parentRows) > 0:
								for prow in parentRows:
									makoLogger.debug('parent_id ({}) status is: {} for svc: {} exec: {}'.format(parent_id, prow[0], vService, vExecId))

								vcontinue = 1
							else:
								makoLogger.debug('Parent is not in complete state yet for svc: {} exec: {}'.format(vService, vExecId))
								vcontinue = 0
						else:
							vcontinue = 1

						if vcontinue == 1:
							if arrFiles[vFileId] != 0:
								makoLogger.debug('arrFiles[{}] != 0 ({}) for svc: {}'.format(vFileId, arrFiles[vFileId], vService))
								vExecs2Files[vExecId] = arrFiles[vFileId]
							else:
								makoLogger.debug('arrFiles[{}] = 0 for svc: {}'.format(vFileId, vService))
								if vExecId in vExecs2Files:
									if vExecs2Files[vExecId] == 0:
										vExecs2Files[vExecId] = 0
								else:
									vExecs2Files[vExecId] = 0
						else:
							vExecs2Files[vExecId] = 3
					else:
						makoLogger.debug('vExecId or vFileId not found in arrExecs or arrFiles for svc: {}'.format(vService))
						vExecs2Files[vExecId] = 3
				else:
					if vExecId in arrExecs:
						makoLogger.debug('vExecId: {} and vFileId: (INACTIVE) found in arrExecs for svc: {}'.format(vExecId, vService))
						parent_id = arrExecs[vExecId]['parent_id']

						vcontinue = 0
						if parent_id != 0:
							query = "SELECT s2e.status FROM svc2execs AS s2e LEFT JOIN services AS svc ON(svc.id = s2e.service_id) WHERE s2e.id = {} AND s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND s2e.last_execution <= CONCAT(DATE(NOW()), ' ', svc.ending_time) AND s2e.enabled = 1 AND s2e.status = 0".format(parent_id)

							makoLogger.debug("Query for svc {}: {}".format(vService, query))
							cursor.execute(query)
							parentRows = cursor.fetchall()

							# If previous query returns any records
							if len(parentRows) > 0:
								for prow in parentRows:
									makoLogger.debug('parent_id ({}) status is: {} for svc: {}'.format(parent_id, prow[0], vService))

								vcontinue = 1
							else:
								makoLogger.debug('Parent is not in complete state yet for svc: {} exec: {}'.format(vService, vExecId))
								vcontinue = 0
						else:
							vcontinue = 1

						if vcontinue == 1:
							if vExecId in vExecs2Files:
								if vExecs2Files[vExecId] == 0:
									vExecs2Files[vExecId] = 0
							else:
								vExecs2Files[vExecId] = 0
						else:
							vExecs2Files[vExecId] = 3
					else:
						makoLogger.debug('vExecId: {} and vFileId: (INACTIVE) not found in arrExecs for svc: {} exec: {}'.format(vExecId, vService, vExecId))

			for vExecId in vExecs2Files:
				if vExecs2Files[vExecId] == 0:
					makoLogger.info('svc2execs id: {} for svc: {} is ready to be picked by this process'.format(vExecId, vService))
					vStatus = "1"
					doUpdate('svc2execs', 'status = ' + vStatus, 'id = ' + str(vExecId))
				#else:
				#	vStatus = "{}".format(vExecs2Files[vExecId])


	makoLogger.info('Finished Finding Execs routine for Service: {}'.format(vService))
	cnx.close()
	cursor.close()

def doExec(svcId):
	#return 0
	# Do database connection
	try:
		cnx = mysql.connector.connect(**sqlconfig)

	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			errorMsg = 'Something is wrong with your DB user name or password'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 21
			raise SystemExit(err.errno)
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			errorMsg = 'Database does not exist'
			makoLogger.critical(errorMsg)
			doEmail('DB Error', errorMsg, 2)
			# 25
			raise SystemExit(err.errno)
		else:
			makoLogger.critical(err)
			# 211 unable to connect
			# 20 Access denied
			doEmail('DB Error', err, 2)
			raise SystemExit(err.errno)

	cursor = cnx.cursor()

	# Query to check SERVICES have downloaded all files and are ready for execution
	# Service Exec Status = Ready to run or Error
	# svc2exec_service_id = service_id
	# Service and Exec are enabled
	# svc2exec_status = Ready to run or Error
	# service_file_status = OK
	# service_starting_time < NOW
	# service_ending_time > NOW
	# Order by service_id and priority Ascendant
	#query = "SELECT s2e.id, s2e.service_id, s2e.program FROM svc2execs s2e, services svc WHERE svc.exec_status IN (1, 3) AND s2e.status IN (1, 3) AND ((svc.id = s2e.service_id AND s2e.enabled = 1 AND svc.enabled = 1 AND CONCAT(DATE(NOW()), ' ', svc.starting_time) < NOW() AND CONCAT(DATE(NOW()), ' ', svc.ending_time) > NOW()) OR s2e.force_run = 1) AND svc.exec_status = 1 AND svc.file_status = 0 AND svc.status != 3 ORDER BY service_id, priority ASC"
	#query = "SELECT s2e.id, s2e.service_id, s2e.program FROM svc2execs s2e, services svc WHERE s2e.status IN (1, 3) AND (((svc.id = s2e.service_id AND s2e.enabled = 1 AND svc.enabled = 1 AND CONCAT(DATE(NOW()), ' ', svc.starting_time) < NOW() AND CONCAT(DATE(NOW()), ' ', svc.ending_time) > NOW()) OR s2e.force_run = 1) AND svc.status != 3) ORDER BY last_execution, service_id, priority ASC"

	#query = "SELECT s2e.id, s2e.service_id, s2e.program FROM svc2execs s2e, services svc WHERE svc.id = s2e.service_id AND s2e.enabled = 1 AND svc.enabled = 1 AND (CONCAT(DATE(NOW()), ' ', svc.starting_time) <= NOW() AND CONCAT(DATE(NOW()), ' ', svc.ending_time) >= NOW() AND s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND (s2e.status = 1 OR s2e.force_run = 1)) ORDER BY s2e.last_execution, s2e.parent_id, s2e.priority ASC LIMIT 1"

	#query = "SELECT s2e.id, s2e.service_id, s2e.program FROM svc2execs s2e, services svc WHERE svc.id = s2e.service_id AND s2e.enabled = 1 AND svc.enabled = 1 AND (CONCAT(DATE(NOW()), ' ', svc.starting_time) <= NOW() AND CONCAT(DATE(NOW()), ' ', svc.ending_time) >= NOW() AND s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND (s2e.status = 1 OR s2e.force_run = 1)) ORDER BY RAND() LIMIT 1"

	query = "SELECT s2e.id, s2e.service_id, s2e.program FROM svc2execs AS s2e, services AS svc WHERE svc.id =s2e.service_id AND s2e.enabled = 1 AND svc.enabled = 1 AND (CONCAT(DATE(NOW()), ' ', svc.starting_time) <= NOW() AND CONCAT(DATE(NOW()), ' ', svc.ending_time) >= NOW() AND s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND (s2e.status = 1 OR s2e.force_run = 1)) AND s2e.service_id = {} ORDER BY s2e.parent_id ASC".format(svcId)

	#makoLogger.debug("Query for checking SERVICES have downloaded all files and are ready for execution: {}".format(query))

	cursor.execute(query)
	allRows = cursor.fetchall()

	# Used to identify when service changes and clean variables properly
	pvSvcId = -1
	pvExecId = -1
	errno = 0

	# If previous query returns any records
	if len(allRows) > 0:
		errorMsg = 'Started Exec Processes for service_id {}'.format(svcId)
		makoLogger.info(errorMsg)
		# Loop through all records
		for row in allRows:
			arrIDs = {}
			vExecId = row[0]
			vSvcId = row[1]
			vExecProg = row[2]
			# service_id and exec_id are going to be used next in this function
			service_id = '{}'.format(vSvcId)
			exec_id = '{}'.format(vExecId)
			arrIDs['service_id'] = vSvcId
			arrIDs['svc2exec_id'] = vExecId
			# args is going to be used for executing the program
			args=shlex.split(vExecProg)

			doUpdate('svc2execs', 'status = 2', 'id = ' + exec_id)

			errorMsg = "Started exec process for service_id: {}, exec_id: {}".format(vSvcId, vExecId)
			doLog(logerrors['Exec'], arrIDs, 2, errorMsg)
			makoLogger.info(errorMsg)

			# Query to save the EXECS TO FILES relation. That way we may have more than a single file per process
			query = "SELECT id, svc2file_id, active FROM execs2files WHERE svc2exec_id = {} AND enabled = 1 ORDER BY id ASC".format(vExecId)
			cursor.execute(query)
			etfRows = cursor.fetchall()
			# If previous query returns any records
			if len(etfRows) > 0:
				# Counter for arrETF
				cnt = 0
				# arrETF is going to store the EXECS to FILES relation
				arrETF = {}
				# arrETF is going to hold
				# arrETF[cnt]['exec2file_id']
				# arrETF[cnt]['file_id']
				# arrETF[cnt]['active']
				# arrETF[cnt]['NPerrno']
				# arrETF[cnt]['NPResult']
				# arrETF[cnt]['ERerrno']
				# arrETF[cnt]['ERResult']

				# Loop through all records
				for etf in etfRows:
					# Number of files to be compared to cnt (Starts counting from 0)
					cntFiles = cnt
					arrETF[cnt] = {}
					arrETF[cnt]['exec2file_id'] = etf[0]
					# It is going to be useful to set files to download again if missing, so not all the files are marked to donwload
					arrETF[cnt]['file_id'] = etf[1]
					# Used to identify if we should care about results given by the execution. Some files are set to be processed but are not in the ftp and always fail. That would cause a big problem
					arrETF[cnt]['active'] = etf[2]
					# Increment counter for next etf
					cnt = cnt + 1

				# Number of files (Starts counting from 1)
				nbrFiles = cnt
			else:
				# If no files are found then break
				makoLogger.error('No files found for service id: {}: exec id: {}'.format(vSvcId, vExecId))
				break
			# Reset cnt counter
			cnt = 0

			# feedUploadService clientNumber runExternalProgram (cancelValue|externalProgram externalParams)
			# 1) vClientNo
			# 2) vExternalExec
			# - vExternalExec == 0
			# --- 3) vCancelParam
			# - vExternalExec == 1
			# --- 3) vExternalExec

			# Get client number from program string
			#vClientNo = re.sub(r'^/home/jmpstart/bin/feedUploadService.sh\s+([0-9]+)\s+([0-1]+)\s+.*$', r'\1', vExecProg)
			vClientNo = args[1]
			# Find if it should execute external programs
			#vExternalExec = re.sub(r'^/home/jmpstart/bin/feedUploadService.sh\s+([0-9]+)\s+([0-1]+)\s+.*$', r'\2', vExecProg)
			vExternalExec = '{}'.format(args[2].strip())

			if vExternalExec == "0":
				# if NOT vExternalExec then execute it internally
				
				vCancelParam = args[3]
				# Get cancel parameter from program string
				#vCancelParam = re.sub(r'^/home/jmpstart/bin/feedUploadService.sh\s+([0-9]+)\s+([0-1]+)\s+(true|false)$', r'\3', vExecProg)
				# Create search string with proper client number and cancel parameter
				vSearchString = "^([0-9]{2}(:|,)+){3}[0-9]{3}\s+INFO\s+\[com.jumpstart.makotek.session.FeedUploadService\]\s+\(http--0.0.0.0-8181-[0-9]+\)\s+The feedUploadService process for client number " + vClientNo + " filetype (ER|NP) with cancel parameter " + vCancelParam + " Result:\s+(Not Exist|successfully|unsuccessfully|has been processed|Yesterday not processed)"
				vTimeString = "^([0-9]{2}(:|,)+){3}[0-9]{3}"
				vExec = args[:]
			else:
				# if vExternalExec then execute externally
				vExternalProg = re.sub(r'^/home/jmpstart/bin/feedUploadService.sh\s+([0-9]+)\s+([0-1]+)\s+(.*)$', r'\3', vExecProg)
				vExec = shlex.split(vExternalProg)

			# String looks like this
			#06:40:04,648 INFO  [com.jumpstart.makotek.session.FeedUploadService] (http--0.0.0.0-8181-2) The feedUploadService process for client number 301 filetype ER with cancel parameter false Result: Not Exist
			# Valid Results
			# NP [true]
			#	Unsuccessfully => OK
			#	Not Exist => File does not exist
			#	has been processed => OK
			#	Yesterday not processed => Error
			# ER [true]
			#	Not Exist => OK
			#	has been processed => OK
			# NP [false]
			#	Successfully => OK
			#	Unsuccessfully => Error
			#	Not Exist => File does not exist
			#	has been processed => OK
			# ER [false]
			#	Not Exist => OK
			#	Unsuccessfully => Error
			#	has been processed => OK

			# If this is the first time it enters the loop
			if pvSvcId == -1:
				pvSvcId = vSvcId

			if pvExecId == -1:
				pvExecId = vExecId

			# If previous service_id is different than current service_id
			if pvSvcId != vSvcId:
				strErrno = '{}'.format(errno)
				strPvSvcId = '{}'.format(pvSvcId)

				# Update SERVICES and SET service_exec_status to the proper value (0 if OK, 3 if ERROR)

				if errno == 0:
					errorMsg = "Execs completed successfully for Service: {}, Exec: {}".format(vSvcId, vExecId)
					doUpdate('svc2execs', 'force_run = 0, status = ' + strErrno, 'id = ' + str(pvExecId))
					doUpdate('services', 'force_run = 0, exec_status = ' + strErrno, 'id = ' + strPvSvcId)
					makoLogger.info(errorMsg)
				else:
					errorMsg = "Execs completed unsuccessfully for Service: {}, Exec: {}".format(vSvcId, vExecId)
					doUpdate('svc2execs', 'status = ' + strErrno, 'id = ' + str(pvExecId))
					doUpdate('services', 'exec_status = ' + strErrno, 'id = ' + strPvSvcId)
					makoLogger.error(errorMsg)
				makoLogger.debug("{}".format(errorMsg))
				doLog(logerrors['Exec'], arrIDs, errno, errorMsg)
				errno = 0
				# Set previous service_id to be the same as current service_id
				pvSvcId = vSvcId

			# Save the current time before program execution. It is going to be used to know which lines to consider from the log file
			vPrevTime = datetime.now() - timedelta(seconds=5)
			#vPrevTime = datetime(2016, 8, 8, 2, 0, 0)
			# Get some variables to create the time for lines in the log
			vtYear = int(vPrevTime.strftime('%Y'))
			vtMonth = int(vPrevTime.strftime('%m'))
			vtDay = int(vPrevTime.strftime('%d'))
			vtThreadStart = int(time.time())
			# Execute program
			try:
				# DESCOMENTAR
				res = subprocess.call(vExec)
				makoLogger.debug(vExec)
			except Exception, e:
				errorMsg = "Error caused by subprocess call {}: {}".format(e.errno, e.strerror)
				makoLogger.error(errorMsg)
				doLog(logerrors['Exec'], arrIDs, errno, errorMsg)
				errno = 3
				doEmail('Exec Error', errorMsg, 2)
			else:
				# Set vCont = 1 to continue reading the file until getting the needed results (NP and ER) for as many files as exec has associated
				if vExternalExec == "0":
					vCont = 1
				else:
					vCont = 0
					errno = 0
					for etf in arrETF:
						arrETF[etf]['NPerrno'] = 0
						arrETF[etf]['NPResult'] = 0
						arrETF[etf]['ERerrno'] = 0
						arrETF[etf]['ERResult'] = 0
						vCancelParam = 'N/A'

				while vCont == 1:
					#makoLogger.debug('Started Checking Loop')
					# Sleep 2 seconds between each iteration
					time.sleep(2)
					# Save NP and ER error codes
					# Reset errno to -1 to store values properly
					for etf in arrETF:
						arrETF[etf]['NPerrno'] = -1
						arrETF[etf]['ERerrno'] = -1
					vNPCont = 1
					vERCont = 1
					with open(INPUTFILE) as fh:
						for line in fh:
							if re.match(vTimeString, line):
								# Get time values from line to compare with time previous to program execution
								aTime = line.split(',', 1)
								aTime = aTime[0].split(':')
								vHour = int(aTime[0])
								vMin = int(aTime[1])
								vSec = int(aTime[2])
								# Create time from line
								try:
									lTime = datetime(vtYear, vtMonth, vtDay, vHour, vMin, vSec)
								except Exception, e:
									errorMsg = "Can't calculate time: {}".format(e)
									makoLogger.error(errorMsg)
								else:
									# If line time is older than time previous to program execution
									if lTime >= vPrevTime:
										if re.match(vSearchString, line):
											# Get file type (NP or ER) from line
											vFileType = re.sub(vSearchString, r'\3', line).strip()
											# Get result code (successfully, unsuccessfully or Not Exist) from line
											vResultCode = re.sub(vSearchString, r'\4', line).strip()
											makoLogger.debug('IMPORTANT: vFileType: {}, vResultCode: {}, vCancelParam: {}'.format(vFileType, vResultCode, vCancelParam))
											# Compare the file type, cancel parameter and result code
											if vCancelParam == 'true':
												if vFileType == 'NP':
													# If current counter is equal to cntFiles count then NP should not be considered
													if cnt == cntFiles:
														vNPCont = 0
													if vResultCode == 'unsuccessfully':
														arrETF[cnt]['NPerrno'] = 0
													elif vResultCode == 'Not Exist':
														arrETF[cnt]['NPerrno'] = 1
#														vNPerrno = 1
													elif vResultCode == 'has been processed':
														arrETF[cnt]['NPerrno'] = 0
													elif vResultCode == 'successfully':
														arrETF[cnt]['NPerrno'] = 0
													elif vResultCode == 'Yesterday not processed':
														arrETF[cnt]['NPerrno'] = 1
														vNPerrno = 1
														errno = 3
														vCont = 0
														makoLogger.debug('Going to break because Yesterday was not processed')
														doUpdate('svc2execs', 'status = 3', 'id = ' + exec_id)
														break
													else:
														arrETF[cnt]['NPerrno'] = 2
													arrETF[cnt]['NPResult'] = vResultCode
													makoLogger.debug('FileType: {}, Cancel: {}, Errno: {}, vResultCode: {}'.format(vFileType, vCancelParam, arrETF[cnt]['NPerrno'], vResultCode))
												else:
													# If current counter is equal to cntFiles count then ER should not be considered
													# NP and ER codes were found. No need to continue reading file
													if cnt == cntFiles:
														vERCont = 0
													if vResultCode == 'Not Exist':
														arrETF[cnt]['ERerrno'] = 0
													elif vResultCode == 'has been processed':
														arrETF[cnt]['ERerrno'] = 0
													elif vResultCode == 'successfully':
														arrETF[cnt]['ERerrno'] = 0
													else:
														arrETF[cnt]['ERerrno'] = 1
													arrETF[cnt]['ERResult'] = vResultCode
													makoLogger.debug('FileType: {}, Cancel: {}, Errno: {}, vResultCode: {}'.format(vFileType, vCancelParam, arrETF[cnt]['ERerrno'], vResultCode))
											else:
												if vFileType == 'NP':
													# If current counter is equal to cntFiles count then NP should not be considered
													if cnt == cntFiles:
														vNPCont = 0
													if vResultCode == 'successfully':
														arrETF[cnt]['NPerrno'] = 0
													elif vResultCode == 'Not Exist':
														arrETF[cnt]['NPerrno'] = 1
													elif vResultCode == 'has been processed':
														arrETF[cnt]['NPerrno'] = 0
													else:
														arrETF[cnt]['NPerrno'] = 2
													arrETF[cnt]['NPResult'] =  vResultCode
													makoLogger.debug('FileType: {}, Cancel: {}, Errno: {}, vResultCode: {}'.format(vFileType, vCancelParam, arrETF[cnt]['NPerrno'], vResultCode))
												else:
													# If current counter is equal to cntFiles count then ER should not be considered
													# NP and ER codes were found. No need to continue reading file
													if cnt == cntFiles:
														vERCont = 0
													if vResultCode == 'Not Exist':
														arrETF[cnt]['ERerrno'] = 0
													elif vResultCode == 'unsuccessfully':
														arrETF[cnt]['ERerrno'] = 1
													elif vResultCode == 'has been processed':
														arrETF[cnt]['ERerrno'] = 0
													elif vResultCode == 'successfully':
														arrETF[cnt]['ERerrno'] = 0
													else:
														arrETF[cnt]['ERerrno'] = 1
													arrETF[cnt]['ERResult'] = vResultCode
													makoLogger.debug('FileType: {}, Cancel: {}, Errno: {}, vResultCode: {}'.format(vFileType, vCancelParam, arrETF[cnt]['ERerrno'], vResultCode))
											# If both arrETF[cnt]['NPerrno|ERerrno'] has been set we can move to next counter
											if arrETF[cnt]['NPerrno'] != -1 and arrETF[cnt]['ERerrno'] != -1:
												cnt = cnt + 1
											# No need to continue reading file. Going to break out of the loop
											if vNPCont == 0 and vERCont == 0:
												vCont = 0
												makoLogger.debug('Going to break')
												break
					if int(vtThreadStart) + maxThreadExecTime < time.time():
						errno = 3
						makoLogger.debug('Going to exit. We have spent over {} seconds waiting for this process to finish {} > {}'.format(maxThreadExecTime, time.time(), vtThreadStart + maxThreadExecTime))
						doUpdate('svc2execs', 'status = 3', 'id = ' + exec_id)
						vCont = 0
						break
												
					#makoLogger.debug('Finished reading file')

				execErrno = 0
				for cnt in range(0, nbrFiles):
					myFileID = '{}'.format(arrETF[cnt]['file_id'])
					arrIDs['exec2file_id'] = '{}'.format(arrETF[cnt]['exec2file_id'])
					arrIDs['svc2file_id'] = myFileID
					if arrETF[cnt]['active'] != 0:
						if arrETF[cnt]['NPerrno'] == 0:
							# NP EXECUTED PROPERLY 'true' => 'unsuccessfully' || 'false' => 'successfully'
							errorMsg = "The Exec for client: {}, Filetype: NP and Cancel Parameter: {} executed properly with Result Code: {}".format(vClientNo, vCancelParam, arrETF[cnt]['NPResult'])
							doLog(logerrors['Exec'], arrIDs, 0, errorMsg)
							makoLogger.info(errorMsg)
							doEmail('Exec Success', errorMsg, 1)
						elif arrETF[cnt]['NPerrno'] == 1:
							# NP FILE IS MISSING 'Not Exist' || Yesterday not processed
							# Update SVC2FILES, SET svc2file_status = 1 (Ready to Run)
							doUpdate('svc2files', 'status = 3', 'id = ' + myFileID)
							doUpdate('services', 'file_status = 3', 'id = ' + service_id)
							try:
								errorMsg = "MISSING FILE {}: The Exec for client: {}, Filetype: NP and Cancel Parameter: {} is in error with Result Code: {}".format(myFileID, vClientNo, vCancelParam, arrETF[cnt]['NPResult'])
								doLog(logerrors['Exec'], arrIDs, 3, errorMsg)
								makoLogger.error(errorMsg)
							except:
								pass
							errno = 3
							execErrno = 3
							doEmail('Exec Error', errorMsg, 2)
						elif arrETF[cnt]['NPerrno'] == 2:
							# NP EXECUTED WITH ERRORS 'false' => 'unsuccessfully'
							errorMsg = "The Exec for client: {}, Filetype: NP and Cancel Parameter: {} is in error with Result Code: {}".format(vClientNo, vCancelParam, arrETF[cnt]['NPResult'])
							doLog(logerrors['Exec'], arrIDs, 3, errorMsg)
							makoLogger.error(errorMsg)
							errno = 3
							execErrno = 3
							doEmail('Exec Error', errorMsg, 2)
						if arrETF[cnt]['ERerrno'] == 0:
							# ER EXECUTED PROPERLY 'Not Exist'
							errorMsg = "The Exec for client: {}, Filetype: ER and Cancel Parameter: {} executed properly with Result Code: {}".format(vClientNo, vCancelParam, arrETF[cnt]['ERResult'])
							doLog(logerrors['Exec'], arrIDs, 0, errorMsg)
							makoLogger.info(errorMsg)
							doEmail('Exec Success', errorMsg, 1)
						elif arrETF[cnt]['ERerrno'] == 1:
							# ER EXECUTED WITH ERRORS 'false' => 'unsuccessfully'
							errorMsg = "The Exec for client: {}, Filetype: ER and Cancel Parameter: {} is in error with Result Code: {}".format(vClientNo, vCancelParam, arrETF[cnt]['ERResult'])
							doLog(logerrors['Exec'], arrIDs, 3, errorMsg)
							makoLogger.error(errorMsg)
							errno = 3
							execErrno = 3
							doEmail('Exec Error', errorMsg, 2)
					else:
						# This file is not active for execution. Do not consider its result value
						try:
							errorMsg = 'Client: {}, Cancel Parameter: {}, File: {} is not active. Omitting checking its status:\n{} - NPResult: '.format(vClientNo, vCancelParam, arrETF[cnt]['file_id'], arrETF[cnt]['NPerrno']);
							errorMsg = '{}{}\n{}'.format(errorMsg, arrETF[cnt]['NPResult'])
							errorMsg = '{}\n{} - ERResult: {}'.format(errorMsg, arrETF[cnt]['ERerrno'], arrETF[cnt]['ERResult'])
							makoLogger.error(errorMsg)
						except:
							pass

				if execErrno == 0:
					# If there were no errors with this exec set status to 0 (OK)
					doUpdate('svc2execs', 'status = 0, force_run = 0', 'id = ' + exec_id)
				else:
					# If there were errors with this exec set status to 3 (ERROR)
					doUpdate('svc2execs', 'status = 3', 'id = ' + exec_id)
		strErrno = '{}'.format(errno)
		# Update SERVICES, SET service_exec_status to the result value (0 = OK, 3 = ERROR)
		if errno == 0:
			errorMsg = "Execs completed successfully for Service: {}, Exec: {}".format(vSvcId, vExecId)
			doUpdate('svc2execs', 'force_run = 0, status = ' + strErrno, 'id = ' + exec_id)
			doUpdate('services', 'force_run = 0, exec_status = ' + strErrno, 'id = ' + service_id)
			makoLogger.info(errorMsg)
			doEmail('Execs Success', errorMsg, 1)
		else:
			errorMsg = "Execs completed unsuccessfully for Service: {}, Exec: {}".format(vSvcId, vExecId)
			doUpdate('svc2execs', 'force_run = 0, status = ' + strErrno, 'id = ' + exec_id)
			doUpdate('services', 'exec_status = ' + strErrno, 'id = ' + service_id)
			makoLogger.error(errorMsg)
			doEmail('Execs Error', errorMsg, 1)
		doLog(logerrors['Exec'], arrIDs, errno, errorMsg)
		makoLogger.info('Finished Exec Process for service_id: {}, exec_id: {}'.format(service_id, exec_id))
	cursor.close()
	cnx.close()
	raise SystemExit(errno)

#def fcnPooler():
if __name__ == '__main__':
	# Save paramiko log to file
	strLogFile = '{}/MakotekFeedsProcessor.{}.log'.format(LogDir, datetime.now().strftime('%Y-%m-%d'))
	makoLogger = logging.getLogger('MakoFeedsProcessor')
	makoLogger.setLevel(logging.DEBUG)
	handlerLogger = logging.FileHandler(strLogFile)
	formatLogger = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
	handlerLogger.setFormatter(formatLogger)
	makoLogger.addHandler(handlerLogger)

	errorMsg = 'Starting Application {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M'))
	doEmail('Application Launch', errorMsg, 0)
	# Command is starting.
	# Update SVC2FILES status to Error if it is set to ready to run or running
	doUpdate('svc2files', 'status = 3', '')
	# Update SVC2FILES status to Error if it is set to running
	doUpdate('services', 'status = 3, file_status = 3, exec_status = 3', 'enabled = 1')
	# Update SVC2EXECS status to Error if it is set to ready to run or running
	doUpdate('svc2execs', 'status = 3', 'status IN(1, 2) AND enabled = 1')

	# This script is going to repeat forever
	while True:
		compareDate = '{}'.format(datetime.now().strftime('%H%M'))
		if compareDate == '0000':
			for hdlr in makoLogger.handlers[:]:
				errorMsg = 'Removing logger handler: {}'.format(hdlr)
				makoLogger.debug(errorMsg)
				makoLogger.removeHandler(hdlr)
			# Save paramiko log to file
			strLogFile = '{}/MakotekFeedsProcessor.{}.log'.format(LogDir, datetime.now().strftime('%Y-%m-%d'))
			handlerLogger = logging.FileHandler(strLogFile)
			formatLogger = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
			handlerLogger.setFormatter(formatLogger)
			makoLogger.addHandler(handlerLogger)

		#print("First pass - {}".format(len(jobs)))
		# For each download job
		for idx in jobs.keys():
			vJob = jobs[idx]
			# If job can join it can be removed
			vJob['job'].join(1)
			if vJob['job'].is_alive():
				if int(vJob['last_execution']) + downloadTime < time.time():
					errorMsg = 'Going to terminate jobs[{}]: {} / {}'.format(idx, vJob, time.time())
					makoLogger.error(errorMsg)
					del svcsPool[idx]
					vJob['job'].terminate()
					vJob['job'].join(1)
					del jobs[idx]
					vUpdate = 'service_id = {} AND status = 2 AND enabled = 1'.format(idx)
					doUpdate('svc2files', 'status = 3', vUpdate)
				else:
					errorMsg = 'Download Status: {} {} / {}'.format(vJob, vJob['job'].is_alive(), time.time())
					makoLogger.debug(errorMsg)
			else:
				errorMsg = 'Removing svcsPool: {} and Jobs: {} / {}'.format(vJob['job'].name, vJob, time.time())
				makoLogger.debug(errorMsg)
				# Remove job form services pool
				#svcsPool.remove(vJob.name)
				del svcsPool[idx]
				# Remove job from job pool
				del jobs[idx]
		# For each job in execs
		for idx in execs.keys():
			# If job can join it can be removed
			vExec = execs[idx]
			vExec['job'].join(1)
			if vExec['job'].is_alive():
				if int(vExec['last_execution']) + maxThreadExecTime < time.time():
					errorMsg = 'Going to terminate vExec[{}]: {} / {}'.format(idx, vExec, time.time())
					makoLogger.error(errorMsg)
					vExec['job'].terminate()
					vExec['job'].join(1)
					del execs[idx]
					vUpdate = 'service_id = {} AND status = 2 AND enabled = 1'.format(idx)
					doUpdate('svc2execs', 'status = 3', vUpdate)
				else:
					errorMsg = 'execs[{}] Status: {} {} / {}'.format(idx, vExec['job'].is_alive(), vExec, time.time())
					makoLogger.debug(errorMsg)
			else:
				errorMsg = 'Removing Exec Job from Pool: {} {}'.format(vExec['name'], vExec)
				makoLogger.debug(errorMsg)
				# Remove job from execs pool
				del execs[idx]
		#print("After first pass removal - {}".format(len(jobs)))

		# Calculate if the time is correct to start
		vNowMin = int(datetime.now().strftime('%M'))
		vContinue = (vNowMin * 60) % sleepTime
		if vContinue == 0:
#			print vNowMin
			# Do database connection
			try:
				cnx = mysql.connector.connect(**sqlconfig)

			except mysql.connector.Error as err:
				if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
					errorMsg = 'Something is wrong with your DB user name or password'
					makoLogger.error(errorMsg)
					doEmail('DB Error', errorMsg, 2)
					# 21
					raise SystemExit(err.errno)
				elif err.errno == errorcode.ER_BAD_DB_ERROR:
					errorMsg = 'Database does not exist'
					makoLogger.error(errorMsg)
					doEmail('DB Error', errorMsg, 2)
					# 25
					raise SystemExit(err.errno)
				else:
					makoLogger.error(err)
					# 211 unable to connect
					# 20 Access denied
					doEmail('DB Error', errorMsg, 2)
					raise SystemExit(err.errno)

			"""
			SERVICES TABLE
			id
			name (Descriptive name)
			description (Description of the service)
			logfile (Where to save the log of the execution process)
			hostname
			port
			username
			password
			starting_time (HH:mm)
			ending_time (HH:mm)
			status (0 => OK, 1 => Ready to Run, 2 => Running, 3 => Error...)
			file_status (0 => OK, 1 => Ready To Run, 2 => Running, 3 => Error...)
			exec_status (0 => OK, 1 => Ready To Run, 2 => Running, 3 => Error...)
			enabled
			force_run (0 => NO, 1 => Force execution not considering time or any other conditions)

			SVC2FILES TABLE
			id
			service_id (SERVICES.service_id)
			description (Description about this file)
			localdir (Local directory where to save file/s)
			remotedir (Remote directory to list to get file/s from)
			filename
			add_extension (String with extension if needed)
			status (0 => OK, 1 => Ready to Run, 2 => Running, 3 => Error...)
			last_execution (Indicates the last time files were processed properly)
			enabled
			force_run (0 => NO, 1 => Force execution not considering time or any other conditions)

			SVC2EXECS TABLE
			id
			service_id (SERVICES.service_id)
			program (Any full path program to execute when SERVICES finishes downloading files)
			priority (Priority to execute programs in order)
			last_execution (Indicates the last time execution was processed)
			enabled
			force_run (0 => NO, 1 => Force execution not considering time or any other conditions)

			SERVICE2LOGS
			id
			datetime (YYYY-MM-DD HH:mm:ss)
			svc2file_id (SVC2FILES.svc2file_id)
			result_code (0 => OK, 1 => Ready to Run, 2 => Running,2 => Error...)
			result_string
			"""

			# Select SERVICES to start based on time
			# service_starting_time < NOW
			# service_ending_time > NOW
			# service_status != Running
			# service_enabled
			# Last exeuction is taking longer than maxThreadExecTime minutes and status = 2 (RUNNING)
			query = "SELECT id, name, logfile, hostname, port, username, password, CONCAT(DATE(NOW()), ' ', starting_time) AS starting_time, CONCAT(DATE(NOW()), ' ', ending_time) AS ending_time, file_status, exec_status FROM services WHERE (enabled = 1 AND CONCAT(DATE(NOW()), ' ', starting_time) <= NOW() AND CONCAT(DATE(NOW()), ' ', ending_time) >= NOW() AND (status != 2 OR (status = 2 AND DATE_ADD(last_execution, INTERVAL {} SECOND) < NOW()))) OR force_run = 1".format(maxThreadExecTime)

			"""
			row[0] => service_id
			row[1] => service_name
			row[2] => service_logfile
			row[3] => service_hostname
			row[4] => service_port
			row[5] => service_username
			row[6] => service_password
			row[7] => service_starting_time
			row[8] => service_ending_time
			row[9] => service_file_status
			row[10] => service_exec_status
			"""

			# arrSvcs is going to be used to store SERVICES and SVC2FILES data to be passed to doDownload function
			arrSvcs = {}

			# svc2filesConditional is going to be used later to look for which files need to be downloaded
			svc2filesConditional = ""
			conditionalSeparator = "WHERE "

			cursor = cnx.cursor()
			cursor.execute(query)
			allRows = cursor.fetchall()

			# If previous query returns any row
			if len(allRows) > 0:
				errorMsg = 'services SELECT returned {} results'.format(len(allRows))
				makoLogger.debug(errorMsg)
				# Go through all results and store them in arrSvcs array
				for row in allRows:
					cur = row[0]
					arrSvcs[cur] = {}
					arrSvcs[cur]['service_id'] = row[0]
					arrSvcs[cur]['service_name'] = row[1]
					arrSvcs[cur]['service_logfile'] = row[2]
					arrSvcs[cur]['service_hostname'] = row[3]
					arrSvcs[cur]['service_port'] = row[4]
					arrSvcs[cur]['service_username'] = row[5]
					arrSvcs[cur]['service_password'] = row[6]
					arrSvcs[cur]['service_starting_time'] = row[7]
					arrSvcs[cur]['service_ending_time'] = row[8]
					arrSvcs[cur]['service_file_status'] = row[9]
					arrSvcs[cur]['service_exec_status'] = row[10]
					arrSvcs[cur]['files'] = {}
					# Files Conditional indicates:
					# File service id = Service id
					# Last execution time < Service start execution time OR Last execution tiem is NULL
					# Last execution > Service start execution time AND File status is not OK or Running
					# Last exeuction is taking longer than 15 minutes and status = 2 (RUNNING)
					# File is enabled					
					svc2filesConditional += "{}((service_id = {} AND ((last_execution < '{}' OR last_execution IS NULL) OR (last_execution > '{}' AND status NOT IN (0, 2)) OR (DATE_ADD(last_execution, INTERVAL 15 MINUTE) < NOW() AND status = 2))) OR force_run = 1)".format(conditionalSeparator, arrSvcs[cur]['service_id'], arrSvcs[cur]['service_starting_time'], arrSvcs[cur]['service_starting_time'])
					conditionalSeparator = " OR "
					findExecs(arrSvcs[cur]['service_id'])

				# Create query to look for files to download
				# Join Files conditional with query to obtain list of files to be downloaded
				query = "SELECT id, service_id, localdir, remotedir, filename, add_extension, last_execution, status, enabled FROM svc2files {}".format(svc2filesConditional)

				#makoLogger.debug(query)
				"""
				row[0] => svc2file_id
				row[1] => svc2file_service_id
				row[2] => svc2file_localdir
				row[3] => svc2file_remotedir
				row[4] => svc2file_filename
				row[5] => svc2file_add_extension
				row[6] => svc2file_last_execution
				row[7] => svc2file_status
				row[8] => enabled
				"""

				# arrFiles is going to be used to store all SVC2FILES data (643)
				arrFiles = {}

				cursor.execute(query)
				allRows = cursor.fetchall()

				errorMsg = 'Files returned {} rows. Threads size: {}'.format(len(allRows), len(jobs))
				makoLogger.debug(errorMsg)
				# If previous query returns any row and the threads have not reached the maximum
				if len(allRows) > 0 and len(jobs) < maxThreads:
					# Go through all results and store them in arrFiles array
					for row in allRows:
						cur = row[0]
						service_id = row[1]
						arrFiles[cur] = {}
						arrFiles[cur]['svc2file_id'] = row[0]
						arrFiles[cur]['svc2file_service_id'] = row[1]
						arrFiles[cur]['svc2file_localdir'] = row[2]
						arrFiles[cur]['svc2file_remotedir'] = row[3]
						arrFiles[cur]['svc2file_filename'] = row[4]
						arrFiles[cur]['svc2file_add_extension'] = row[5]
						arrFiles[cur]['svc2file_last_execution'] = row[6]
						arrFiles[cur]['svc2file_status'] = row[7]
						arrFiles[cur]['svc2file_enabled'] = row[8]
						# Store recently created arrFiles into arrSvcs
						arrSvcs[service_id]['files'][cur] = arrFiles[cur]

					# Go through all results in arrFiles
					for vFile in arrFiles:
						svcId = arrFiles[vFile]['svc2file_service_id']
						# If threads have not reached the maximum
						if len(jobs) < maxThreads:
							# Check if this service is in svcsPool
							if svcId in svcsPool:
								if int(jobs[svcId]['last_execution']) + downloadTime < time.time():
									errorMsg = 'Going to terminate jobs[{}]: {} / {}'.format(svcId, jobs[svcId], time.time())
									makoLogger.error(errorMsg)
									del svcsPool[svcId]
									jobs[svcId]['job'].terminate()
									jobs[svcId]['job'].join(1)
									del jobs[svcId]
									vUpdate = 'service_id = {} AND status = 2 AND enabled = 1'.format(svcId)
									doUpdate('svc2files', 'status = 3', vUpdate)
								else:
									errorMsg = 'Omitting arrFiles: {}, because svcsPool contains id: {} last_execution: {} / {}'.format(arrFiles[vFile]['svc2file_id'], svcId, jobs[svcId]['last_execution'], time.time())
									makoLogger.debug(errorMsg)
							else:
								errorMsg = 'Starting doDownload process for Service: {}'.format(svcId)
								makoLogger.debug(errorMsg)
								# Append service to svcsPool array
								svcsPool[svcId] = svcId
								valToPass = {}
								valToPass = arrSvcs[svcId]
								# Start doDownload process with name service_id passing arrSvcs[svcId] array
								vProcess = multiprocessing.Process(target=doDownload, name=svcId, args=(valToPass,))
								vProcess.daemon = True
								# Append process to jbos array
								jobs[svcId] = {}
								jobs[svcId]['name'] = svcId
								jobs[svcId]['job'] = vProcess
								jobs[svcId]['last_execution'] = time.time()
								vProcess.start()
						else:
							errorMsg = 'Job\'s size {} is longer the maxThreads\' size {}'.format(len(jobs), maxThreads)
							#print("Second pass - len(jobs) > maxThreads - {}".format(len(jobs)))
							# For each download job
							for idx in jobs.keys():
								vJob = jobs[idx]
								if int(vJob['last_execution']) + downloadTime < time.time():
									errorMsg = 'Going to terminate jobs[{}]: {} / {}'.format(idx, vJob, time.time())
									makoLogger.error(errorMsg)
									del svcsPool[idx]
									vJob['job'].terminate()
									vJob['job'].join(1)
									del jobs[idx]
									vUpdate = 'service_id = {} AND status = 2 AND enabled = 1'.format(idx)
									doUpdate('svc2files', 'status = 3', vUpdate)
								else:
									# If job can join it can be removed
									vJob['job'].join(1)
									if vJob['job'].is_alive():
										errorMsg = 'Download Status: {} {}'.format(vJob['job'], vJob['job'].is_alive())
										makoLogger.debug(errorMsg)
									else:
										errorMsg = 'Removing svcsPool: {} and Jobs: {}'.format(vJob['job'].name, vJob['job'])
										makoLogger.debug(errorMsg)
										# Remove job form services pool
										del svcsPool[idx]
										# Remove job form job pool
										del jobs[idx]
							# If threads have not reached maximum
							if len(jobs) < maxThreads:
								# Check if this service is in svcsPool
								if svcId in svcsPool:
									if int(jobs[svcId]['last_execution']) + downloadTime < time.time():
										errorMsg = 'Going to terminate jobs[{}]: {} / {}'.format(svcId, jobs[svcId], time.time())
										makoLogger.error(errorMsg)
										del svcsPool[svcId]
										jobs[svcId]['job'].terminate()
										jobs[svcId]['job'].join(1)
										del jobs[svcId]
										vUpdate = 'service_id = {} AND status = 2 AND enabled = 1'.format(svcId)
										doUpdate('svc2files', 'status = 3', vUpdate)
									else:
										errorMsg = 'Omitting arrFiles: {}, because svcsPool contains id: {} last_execution {} / {}'.format(arrFiles[vFile]['svc2file_id'], svcId, jobs[svcId]['last_execution'], time.time())
										makoLogger.debug(errorMsg)
								else:
									errorMsg = 'Starting doDownload process for Service: {}'.format(svcId)
									makoLogger.debug(errorMsg)
									# Append service to svcsPool
									svcsPool[svcId] = svcId
									valToPass = {}
									valToPass = arrSvcs[svcId]
									# Start doDownload process with name service_id passing arrSvcs[svcId] array
									vProcess = multiprocessing.Process(target=doDownload, name=svcId, args=(valToPass,))
									vProcess.daemon = True
									# Append process to jobs array
									jobs[svcId] = {}
									jobs[svcId]['name'] = svcId
									jobs[svcId]['job'] = vProcess
									jobs[svcId]['last_execution'] = time.time()
									vProcess.start()
							else:
								makoLogger.warn('All threads are busy at the moment. Will sleep and try again later')

			for execNum in range(maxExecs):
				svcExclude = ""
				if len(execs) > 0:
					exclSeparator = "AND svc.id NOT IN("
					for svcId in execs.keys():
						svcExclude += exclSeparator + "{}".format(svcId)
						exclSeparator = ", "
					svcExclude += ")"

				query = "SELECT svc.id FROM svc2execs AS s2e LEFT JOIN services AS svc ON(svc.id = s2e.service_id AND s2e.last_execution >= CONCAT(DATE(NOW()), ' ', svc.starting_time) AND s2e.last_execution <= CONCAT(DATE(NOW()), ' ', svc.ending_time) AND s2e.status = 1) WHERE svc.enabled = 1 AND s2e.enabled = 1 {} GROUP BY svc.id ORDER BY RAND() LIMIT 1".format(svcExclude)
				cursor.execute(query)
				allRows = cursor.fetchall()
				if len(allRows) > 0 and len(execs) < maxExecs:
					for row in allRows:
						svcId = row[0]
						if not svcId in execs.keys():
							errorMsg = 'Exec\'s size is {}. Starting doExec process for service_id: {}'.format(len(execs), svcId)
							makoLogger.debug(errorMsg)
							vProcess = multiprocessing.Process(target=doExec, name=doExec, args=(svcId,))
							vProcess.daemon = True
							# Append process to execs array
							execs[svcId] = {}
							execs[svcId]['name'] = svcId
							execs[svcId]['job'] = vProcess
							execs[svcId]['last_execution'] = time.time()
							vProcess.start()
						else:
							errorMsg = 'Already executing doExec process for service_id: {}'.format(svcId)
							makoLogger.debug(errorMsg)
				else:
					if len(execs) == maxExecs:
						errorMsg = 'Already executing max number of execs'
						makoLogger.debug(errorMsg)
				time.sleep(5)
			cursor.close()
			cnx.close()

#		errorMsg = '{} - Going to wait for next execution'.format(datetime.now().strftime('%H%M%S'))

		# Calculate how many seconds should sleep for next execution window
		vMod = (int(datetime.now().strftime('%M')) * 60) % sleepTime
		makoLogger.debug(vMod)
		if vMod == 0:
			errorMsg = 'Going to wait {} seconds until next execution'.format(sleepTime - int(datetime.now().strftime('%S')))
			makoLogger.info(errorMsg)
			time.sleep(sleepTime - int(datetime.now().strftime('%S')))
		else:
			errorMsg = 'Going to wait {} seconds until next execution'.format(sleepTime - vMod - int(datetime.now().strftime('%S')))
			makoLogger.info(errorMsg)
			time.sleep(sleepTime - vMod - int(datetime.now().strftime('%S')))
raise SystemExit(0)
